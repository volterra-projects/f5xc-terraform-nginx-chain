resource "volterra_origin_pool" "origin_pool" {
  name        = format("%s-origin", var.resource_prefix)
  namespace   = var.namespace
  description = format("Origin pool pointing to origin server public domain")
  labels = {
    "ves.io/app_type" = volterra_app_type.chain-app-type.name
  }
  loadbalancer_algorithm = "ROUND ROBIN"
  healthcheck {
    name      = volterra_healthcheck.http_hc.name
    namespace = var.namespace
  }
  origin_servers {
    private_name {
      dns_name        = var.origin_dns_name
      outside_network = true
      site_locator {
        virtual_site {
          name = volterra_virtual_site.vk8s_virtual_site.name
        }
      }
    }
    # public_name {
    #   dns_name = var.origin_dns_name
    # }
  }
  automatic_port = true
  use_tls {
    tls_config {
      default_security = true
    }
  }
  endpoint_selection = "LOCAL_PREFERRED"
}

resource "volterra_origin_pool" "vk8s_nginx_pool" {
  name      = "${var.resource_prefix}-vk8s-origin"
  namespace = var.namespace
  labels = {
    "ves.io/app_type" = volterra_app_type.chain-app-type.name
  }
  loadbalancer_algorithm = "ROUND ROBIN"
  origin_servers {
    k8s_service {
      vk8s_networks = true
      service_name  = "${kubernetes_service.nginx-service.metadata.0.name}.${kubernetes_service.nginx-service.metadata.0.namespace}"
      site_locator {
        virtual_site {
          name = volterra_virtual_site.vk8s_virtual_site.name
        }
      }
    }
  }
  healthcheck {
    name      = volterra_healthcheck.http_hc.name
    namespace = var.namespace
  }
  same_as_endpoint_port = true
  port                  = 8080
  no_tls                = true
  endpoint_selection    = "LOCAL_PREFERRED"


}

