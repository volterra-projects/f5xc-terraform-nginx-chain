locals {
  public_dns_name = format("%s-%s", var.resource_prefix, var.public_dns_name)
}

resource "volterra_http_loadbalancer" "app_public_lb" {

  lifecycle {
    ignore_changes = [labels]
  }
  name        = format("%s-app-public-lb", var.resource_prefix)
  namespace   = var.namespace
  description = format("Public Http loadbalancer object for %s service", var.resource_prefix)
  domains     = [local.public_dns_name]
  labels = {
    "ves.io/app_type" = volterra_app_type.chain-app-type.name
  }

  advertise_on_public_default_vip = true
  https_auto_cert {
    http_redirect         = true
    no_mtls               = true
    add_hsts              = false
    default_header        = true
    enable_path_normalize = true
    tls_config {
      default_security = true
    }
    port = "443"
  }

  default_route_pools {
    pool {
      name      = volterra_origin_pool.origin_pool.name
      namespace = var.namespace
    }
    weight   = 1
    priority = 1
  }
  round_robin = true

  routes {
    simple_route {
      path {
        prefix = "/nginx"
      }
      origin_pools {
        pool {
          name = volterra_origin_pool.vk8s_nginx_pool.name
        }
      }
      advanced_options {
        prefix_rewrite = "/"
      }
    }
  }

  # ## DDOS
  # enable_ddos_detection {
  #   enable_auto_mitigation {
  #     block = true
  #   }
  # }

  ## General security
  disable_rate_limit              = true
  disable_trust_client_ip_headers = true
  user_id_client_ip               = true
  add_location                    = true

  ## WAF
  app_firewall {
    name      = volterra_app_firewall.waf.name
    namespace = var.namespace
  }

}

data "volterra_http_loadbalancer_state" "public-lb-state" {
  name      = volterra_http_loadbalancer.app_public_lb.name
  namespace = var.namespace
}

resource "volterra_http_loadbalancer" "vk8s_internal_lb" {

  name      = "${var.resource_prefix}-vk8s-internal-lb"
  namespace = var.namespace
  labels = {
    "ves.io/app_type" = volterra_app_type.chain-app-type.name
  }

  domains = ["vk8s-internal-lb.local"]

  http {
    port = 8080
  }

  advertise_custom {
    advertise_where {
      vk8s_service {
        virtual_site {
          name = volterra_virtual_site.vk8s_virtual_site.name
        }
      }
      port = 8080
    }
  }

  default_route_pools {
    pool {
      name      = volterra_origin_pool.origin_pool.name
      namespace = var.namespace
    }
    weight   = 1
    priority = 1
  }

}
