# F5XC Terraform Nginx Chain

This Terraform script creates a public HTTP Loadbalancer in XC Regional edges and deploys a distributed vk8s namespace where an nginx instance is running and is inserted into the data path.

When using the `/nginx` route the request is proxied through the Nginx instance to the defined upstream origin, if `/` is used then the request goes directly to the upstream origin.

```mermaid
flowchart LR;
    A[Client] <-->|/| B(Upstream Origin);
    A <-->|/nginx| C(NGINX vk8s instance);
    C <--> B;
```

The Nginx instance by default is doing some string substitution to insert html in the response body.
The Nginx configuration template is in the `/conf` folder.


See https://registry.terraform.io/providers/volterraedge/volterra/latest/docs for volterra (now F5XC) terraform provider docs.

> Note - Due to dockerhubs rate-limiting you need to provide dockerhub account details in order to pull the container to the distributed vk8s environment.

## Demo steps
1. Create a F5XC api credential (see https://docs.cloud.f5.com/docs/how-to/user-mgmt/credentials)
2. Download and put the .p12 file in the **/creds** folder
3. Create an **VES_P12_PASSWORD** environment variable with your credential password entered in F5 XC GUI/API during creation:

    Example (Linux):
    ```
    read -s VES_P12_PASSWORD && export VES_P12_PASSWORD
    ```
4. Create a **terraform.tfvars** file in the main folder
    
    Example

    ```
    api_p12_file = "./creds/<YOUR FILE>.p12"
    api_url       = "https://<TENANT NAME>.console.ves.volterra.io/api"

    resource_prefix = "<INITIALS>-nginx-chain"
    namespace = "<VOLTERRA NAMESPACE>"

    public_dns_name = "<PUBLIC DOMAIN NAME FOR XC LOADBALANCER>"
    origin_dns_name = "<PUBLIC DOMAIN NAME FOR ORIGIN SERVER>"

    registry_username = "<DOCKERHUB_USER>"
    registry_password = "<DOCKERHUB_PASSWORD>"
    registry_email    = "<DOCKERHUB_EMAIL>"
    ```
5. `terraform init`
6. `terraform apply`

> `terraform destroy` when done.


