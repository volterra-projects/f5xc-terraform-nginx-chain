// VK8S

//Consistency issue with vk8s resource response
//https://github.com/volterraedge/terraform-provider-volterra/issues/54
resource "time_sleep" "vk8s_wait" {
  depends_on      = [volterra_virtual_k8s.vk8s]
  create_duration = "120s"
}


resource "volterra_virtual_k8s" "vk8s" {
  name      = format("%s-vk8s", var.resource_prefix)
  namespace = var.namespace

  vsite_refs {
    name      = volterra_virtual_site.vk8s_virtual_site.name
    namespace = var.namespace
  }
  # vsite_refs {
  #   name      = ""
  #   namespace = var.namespace
  # }
}

resource "volterra_api_credential" "vk8s_cred" {
  name                  = "${var.resource_prefix}-api-cred"
  api_credential_type   = "KUBE_CONFIG"
  virtual_k8s_namespace = var.namespace
  virtual_k8s_name      = volterra_virtual_k8s.vk8s.name
  expiry_days           = var.cred_expiry_days
  depends_on            = [time_sleep.vk8s_wait]
}

resource "local_file" "kubeconfig" {
  content = base64decode(volterra_api_credential.vk8s_cred.data)
  # filename = format("%s/creds/%s", abspath(path.root), format("%s-vk8s.yaml", terraform.workspace))
  filename = "${abspath(path.root)}/creds/${terraform.workspace}-vk8s.yaml"
}
