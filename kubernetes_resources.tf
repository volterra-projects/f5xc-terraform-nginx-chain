resource "kubernetes_deployment" "nginx" {
  depends_on = [local_file.kubeconfig, volterra_http_loadbalancer.vk8s_internal_lb]
  timeouts {
    create = "2m"
  }
  metadata {
    namespace = var.namespace
    name      = "${var.resource_prefix}-nginx-deployment"
    labels = {
      app               = "nginx"
      "ves.io/app_type" = volterra_app_type.chain-app-type.name
    }
    annotations = {
      "ves.io/workload-flavor" = var.vk8s_workload_flavor

    }
  }

  spec {
    replicas = 1
    selector {
      match_labels = {
        app = "nginx"
      }
    }

    template {
      metadata {
        labels = {
          app = "nginx"
        }
      }

      spec {
        container {
          image = "nginxinc/nginx-unprivileged:1.25.4"
          name  = "nginx"

          volume_mount {
            name       = "config-volume"
            mount_path = "/etc/nginx/conf.d"
          }

          port {
            container_port = 8080
          }

          liveness_probe {
            http_get {
              path = "/"
              port = 8080

            }

            initial_delay_seconds = 3
            period_seconds        = 15
          }
        }

        image_pull_secrets {
          name = kubernetes_secret.dockerhub-secret.metadata.0.name
        }

        volume {
          name = "config-volume"

          config_map {
            name = kubernetes_config_map.nginx_conf.metadata.0.name
            items {
              key  = "custom-nginx.conf"
              path = "custom-nginx.conf"
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "nginx-service" {
  depends_on = [local_file.kubeconfig]
  metadata {
    namespace = var.namespace
    name      = "${var.resource_prefix}-nginx-service"
    labels = {
      "ves.io/app_type" = volterra_app_type.chain-app-type.name
    }
  }
  spec {
    selector = {
      app = kubernetes_deployment.nginx.metadata.0.labels.app
    }
    type = "ClusterIP"
    port {
      port        = 8080
      target_port = 8080
    }
  }

}

resource "kubernetes_config_map" "nginx_conf" {
  depends_on = [local_file.kubeconfig]
  metadata {
    name      = "${var.resource_prefix}-nginx-config"
    namespace = var.namespace
  }

  data = {
    "custom-nginx.conf" = templatefile("${path.module}/conf/custom-nginx.tftpl", {
      internal_vk8s_lb = "http://${volterra_http_loadbalancer.vk8s_internal_lb.domains.0}:8080/"
    })
  }

}

resource "kubernetes_secret" "dockerhub-secret" {
  depends_on = [local_file.kubeconfig]
  metadata {
    name      = "${var.resource_prefix}-docker-cfg"
    namespace = var.namespace
  }

  type = "kubernetes.io/dockerconfigjson"

  data = {
    ".dockerconfigjson" = jsonencode({
      auths = {
        "${var.registry_server}" = {
          "username" = var.registry_username
          "password" = var.registry_password
          "email"    = var.registry_email
          "auth"     = base64encode("${var.registry_username}:${var.registry_password}")
        }
      }
    })
  }

}
