variable "api_p12_file" {
  type        = string
  description = "Volterra API p12 file path"
}

variable "api_url" {
  type        = string
  description = "Volterra tenant api url"
  default     = "https://yourtenant.console.ves.volterra.io/api"
}

variable "resource_prefix" {
  type        = string
  description = "Object Prefix"
}

variable "namespace" {
  type        = string
  description = "Volterra namespace to use"
}

variable "public_dns_name" {
  type        = string
  description = "app public domain name, resource_prefix will be added"
}

variable "origin_dns_name" {
  type        = string
  description = "origin app public domain name"
}

variable "virtual_site_selector_expression" {
  type        = list(string)
  description = "selector expression to select REs for virtual site"
  default     = ["ves.io/siteType in (ves-io-re)"]
}

variable "vk8s_workload_flavor" {
  type        = string
  description = "vk8s workload flavor (tiny, medium or large)"
  default     = "tiny"
}

variable "cred_expiry_days" {
  type        = number
  description = "The number of days the Volterra kubernetes API credentials are valid"
  default     = 60
}

variable "registry_email" {
  type        = string
  description = "Container registry email"
}
variable "registry_password" {
  type        = string
  description = "Container registry password"
}
variable "registry_username" {
  type        = string
  description = "Container registry username"

}
variable "registry_server" {
  type        = string
  description = "Container registry url"
  default     = "https://index.docker.io/v1/"

}
