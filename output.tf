output "public_dns_name" {
  value = volterra_http_loadbalancer.app_public_lb.domains.0
}

output "public_lb_cert_state" {
  value = data.volterra_http_loadbalancer_state.public-lb-state.state

  # lifecycle {
  #   precondition {
  #     condition = data.volterra_http_loadbalancer_state.public-lb-state.state == "VIRTUAL_HOST_READY"
  #   }
  # }
}
