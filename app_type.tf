resource "volterra_app_type" "chain-app-type" {
  namespace = "shared"
  name      = "nginx-chain-app"

  features {
    type = "BUSINESS_LOGIC_MARKUP"
  }
  features {
    type = "TIMESERIES_ANOMALY_DETECTION"
  }
  features {
    type = "PER_REQ_ANOMALY_DETECTION"
  }
  features {
    type = "USER_BEHAVIOR_ANALYSIS"
  }
}

resource "volterra_app_setting" "chain-app-settings" {
  name      = "chain-app-settings"
  namespace = var.namespace

  app_type_settings {
    app_type_ref {
      name      = volterra_app_type.chain-app-type.name
      namespace = volterra_app_type.chain-app-type.namespace
    }

    business_logic_markup_setting {
      disable = true
    }

    timeseries_analyses_setting {
      metric_selectors {
        metric         = ["REQUEST_RATE", "ERROR_RATE", "LATENCY", "THROUGHPUT"]
        metrics_source = "VIRTUAL_HOSTS"
      }
    }

    user_behavior_analysis_setting {
      disable_learning = true

      disable_detection = true
    }
  }
}
