resource "volterra_virtual_site" "vk8s_virtual_site" {
  name      = "${var.resource_prefix}-vk8s-virtual-site"
  namespace = var.namespace
  site_type = "REGIONAL_EDGE"
  site_selector {
    expressions = var.virtual_site_selector_expression
  }

}
